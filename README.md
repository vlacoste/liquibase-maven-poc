# Liquibase-maven-poc

## Prerequisite

### Maven/java installed

Just install maven/java if not already down

### Docker

Use docker and docker-compose to provision a postgres database server and a pgadmin client to see graphically the DB

To to that, just use the provided docker-compose file: `docker-compose.yml` and launch it in a terminal with
the command: `$ docker-compose up`

## Check postgres & pgadmin

- Check if you can access to the database with pgadmin by opening in a browser: `http://localhost:5050/`
- use following user/password : raj@nola.com/admin
- create a new connection to the created database with the following parameters: 
    - host: local_pgdb
    - post: 5432
    - maintenance database: postgres
    - user: user
    - password: admin
- you can now normally see the DB inside

to access host from inside container on MacOs => use name `host.docker.internal`

NB: it's possible to access to the DB in command line with `psql` like this: 
`PGPASSWORD=admin psql -h 127.0.0.1 -p 54320 -U user`

## Liquibase command:

### use of liquibase

Liquibase can be used with an executable or with maven and a liquibase plugin
We prefer here to use the maven liquibase plugin but if you prefer you can use the liquibase binary (exist on all environments Linux/Mac OS/ Windows)

To do that, we've provisioned in the code base a `liquibase.properties` file that contain information to permit liquibase to access to database like url, user, password and driver

Now with maven, to create the table defined in `db-changelog.yaml` liquibase file, just do :
`mvn liquibase:update`

it will apply then all changed set defined in `db-changelog.yaml`

After to rollback changsets, it's possible :
- to specify how many changesets we desire to rollback with the command: `mvn liquibase:rollback -Dliquibase.rollbackCount=<n>` where `n` is the desired count 
- to specify the chageset tag we desire to rollback to with the command: `mvn liquibase:rollback -Dliquibase.rollbackTag=<tag>` when `tag` is the desired tag

### Generate  a rollback SQL file

Instead of rollbacking directly the database, it's also possible to generate SQL file that will be used after with the command:
- `mvn liquibase:rollbackSQL -Dliquibase.rollbackCount=<n>`
- `mvn liquibase:rollbackSQL -Dliquibase.rollbackTag=<tag>`

